//
//  Colors.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 16/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit



extension UIView {
    func setBackgroundColor() {
        backgroundColor = .systemGray5
    }
}
