//
//  InfoView.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 16/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

struct StringSettings {
    let disclimerTitle = NSLocalizedString("Disclaimer", comment: "")
    let aboutTitle = NSLocalizedString("About", comment: "")
    
    let aboutDescription = """

                            Aplikacja ma na celu w prosty sposób pomóc użytkownikowi spełnić wymagania postawione przez rząd w związku z zakazem przemieszczania się. Zaleca się aby nie wychodzić z domu na odległość dalszą jak 2km od swojego miejsca zamieszkania.

                            W dniu 10.04.2020 r. rząd ogłosił, że obowiązujące ograniczenia pozostają utrzymane do dnia 05.05.2020 r.  Głównym zaleceniem jest, aby wszystkie osoby pozostały w domu.

                            Dozwolone jest wyjście w poniższych przypadkach:

                            • podróż do i z pracy, w celach służbowych; lista tzw. ważnych sektorów jest dostępna tutaj.
                            • w celu zakupu żywności, artykułów gospodarstwa domowego lub odbioru zamówionych posiłków;
                            • w celu odbycia wizyt lekarskich, obioru lekarstw;
                            • z ważnych powodów rodzinnych takich jak opieka na dziećmi, osobami starszymi lub schorowanymi;
                            • w celu odbycia ćwiczeń w odległości 2 km od miejsca zamieszkania; w ćwiczeniach mogą uczestniczyć małoletni pod warunkiem przestrzegania zasad bezpiecznego odstępu;
                            • rolnicy w celach produkcji żywności i opieki nad zwierzętami.


                            1. OGRANICZENIA W PRZEMIESZCZANIU SIĘ

                            Ograniczenie dotyczy: przemieszczania się, podróżowania, przebywania w miejscach publicznych.

                            Na czym polega? Wychodzenie z domu powinno być ograniczone do absolutnego minimum, a odległość utrzymana od innych pieszych powinna wynosić co najmniej 2 metry.

                            Przemieszczać się możesz w przypadku:

                            dojazdu do i z pracy (dotyczy to także zakupu towarów i usług związanych z zawodową działalnością),
                            wolontariatu na rzecz walki z COVID-19 (dotyczy to pomocy osobom przebywającym na kwarantannie lub osobom, które nie powinny wychodzić z domu),
                            załatwiania spraw niezbędnych do życia codziennego (do czego zalicza się np., niezbędne zakupy, wykupienie lekarstw, wizyta u lekarza, opieka nad bliskimi),
                            wykonywania czynności związanych z realizacją zadań określonych w ustawie – Prawo łowieckie i ustawie o ochronie zdrowia zwierząt i zwalczaniu chorób zakaźnych zwierząt oraz zakupu towarów i usług z nimi związanych.
                            

                            Ważne! 2 metry – minimalna odległość między pieszymi

                            Wprowadzamy obowiązek utrzymania co najmniej 2-metrowej odległości między pieszymi. Dotyczy to także rodzin i bliskich.

                            Wyłączeni z tego obowiązku są:

                            rodzice z dziećmi wymagającymi opieki (do 13 roku życia),
                            a także osoby niepełnosprawne, niemogące się samodzielnie poruszać, osoby z orzeczeniem o potrzebie kształcenia specjalnego i ich opiekunowie.
                             

                            2. ZAKAZ WYCHODZENIA NA ULICĘ NIELETNICH BEZ OPIEKI DOROSŁEGO

                            Ograniczenie dotyczy: przebywania osób do 18 roku życia poza domem bez opieki dorosłego

                            Na czym polega? Dzieci i młodzież, które nie ukończyły 18 roku życia, nie mogą wyjść z domu bez opieki. Tylko obecność rodzica, opiekuna prawnego lub kogoś dorosłego usprawiedliwia ich obecność na ulicy i tylko w określonych przypadkach:

                            dojazdu do i z pracy,
                            wolontariatu na rzecz walki z COVID-19,
                            czy też załatwiania spraw niezbędnych do życia codziennego.
                            

                            czytaj dalej:
                            """
    
    let disclimerDescription = """
                               Copyright (c) 2020 Matt Kopacz

                               Permission is hereby granted, free of charge, to any person obtaining a copy
                               of this software and associated documentation files (the "Software"), to deal
                               in the Software without restriction, including without limitation the rights
                               to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
                               copies of the Software, and to permit persons to whom the Software is
                               furnished to do so, subject to the following conditions:

                               The above copyright notice and this permission notice shall be included in all
                               copies or substantial portions of the Software.

                               THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                               IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                               FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
                               AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                               LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                               OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
                               SOFTWARE.
                               """
}


class InfoView: UIView {
    
    
    
    
    let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        return label
    }()
    
    let infoText: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16, weight: .light)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .systemGray5
        textView.textColor = .label
        textView.isEditable = false
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setConstraints()
        setBackgroundColor()
    }
    
    var settingsType: SettingsViewModelType? {
        didSet {
            guard let type = settingsType else { return }
            let stringSettings = StringSettings()
            switch type {
            case .desclimer:
                title.text = stringSettings.disclimerTitle
                infoText.text = stringSettings.disclimerDescription
            case .about:
                title.text = stringSettings.aboutTitle
                let attributedString = NSMutableAttributedString(string: stringSettings.aboutDescription, attributes: [.foregroundColor: UIColor.label])
                let govAddress = NSAttributedString(string: " Zasady i ograniczenia \n", attributes: [NSAttributedString.Key.link: URL(string: "https://www.gov.pl/web/koronawirus/aktualne-zasady-i-ograniczenia")!])
               
                attributedString.append(govAddress)
                infoText.attributedText = attributedString
             
                
            default:
                break
            }
            
        }
    }
    
    func setConstraints(){
        addSubview(title)
        addSubview(infoText)
        
        
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            title.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 16),
            title.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            
            
            infoText.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            infoText.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 16),
            infoText.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            infoText.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -60),
            
        ])
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
