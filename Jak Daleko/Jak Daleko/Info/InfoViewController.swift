//
//  InfoViewController.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 16/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    var settingsType: SettingsViewModelType?
    private var infoView: InfoView!
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemGray5
        infoView = InfoView(frame: view.frame)
        view.addSubview(infoView)
        infoView.settingsType = settingsType
    }
    

}
