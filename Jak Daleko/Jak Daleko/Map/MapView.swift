//
//  MapView.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 14/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit
import MapKit

class MapView: UIView {

    
    let map: MKMapView = {
       let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.showsCompass = true
        mapView.showsUserLocation = true
        mapView.mapType = .standard
        return mapView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setConstraints()
        backgroundColor = .systemGray5
    }
    
    func setConstraints(){
        addSubview(map)
        NSLayoutConstraint.activate([
            
            map.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            map.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            map.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            map.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
            
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
