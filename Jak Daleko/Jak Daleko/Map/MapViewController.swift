//
//  ViewController.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 14/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    

    var locationManager: CLLocationManager?
    var distanceInKM: Int = 1
    var mapView: MapView!
    var markButtonView: MarkButtonView!
    var spanRegion:Bool = true 
    var pinLocation: CLLocationCoordinate2D?
    var radius: Double = 2000.0
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Jak daleko"
  
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        //locationManager?.requestAlwaysAuthorization()
        locationManager?.requestLocation()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        mapView = MapView(frame: view.frame)
        locationManager?.startMonitoringSignificantLocationChanges()
        
        locationManager?.pausesLocationUpdatesAutomatically = false
//        notificationCenter = UNUserNotificationCenter.current()
//        notificationCenter?.requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
//        }
        
        view.addSubview(mapView!)
        
        mapView.map.delegate = self
        
        
        
        
        
        
        let viewWidth = view.frame.size.width
        let viewHeight = view.frame.size.height
        

        markButtonView = MarkButtonView(frame: CGRect(x: viewWidth/2 - viewWidth/3, y: viewHeight - viewWidth/2, width: viewWidth/1.5, height: viewWidth/5))


        
        view.addSubview(markButtonView!)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(markYouPosition))
        markButtonView.addGestureRecognizer(tapGestureRecognizer)
        setNavigatinBar()
        
       
        
      
        
        
           
        
        
         
     
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let mapType = MapType(rawValue: UserDefaults.standard.integer(forKey: "mapType"))
        let tempRadius =  UserDefaults.standard.double(forKey: "radius")
        if tempRadius > 0.0 {
            radius = tempRadius
        }
        let savedLatitude = UserDefaults.standard.double(forKey: "curretnLatitude")
        let savedLongitude = UserDefaults.standard.double(forKey: "curretnLongitude")
        
        if savedLatitude != 0.0 && savedLongitude != 0.0 {
            let pinLocation = CLLocationCoordinate2D(latitude: savedLatitude, longitude: savedLongitude)
            drawCircle(location: pinLocation)
            markButtonView.titleLabel.text = NSLocalizedString("Remove location", comment: "") 
            UserDefaults.standard.set(true, forKey: "stopUpdate")
            
        }
        
        switch mapType {
        case .standard:
            mapView.map.mapType = .standard
        case .mutedStandard:
            mapView.map.mapType = .mutedStandard
        case .hybrid:
            mapView.map.mapType = .hybrid
        case.satellite:
            mapView.map.mapType = .satellite
        default:
            break
        }
    }
    

    func setNavigatinBar(){
        navigationController?.navigationBar.barTintColor = .systemGray5
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        let settingButton = UIBarButtonItem(image: UIImage(systemName: "gear")?.withTintColor(.systemIndigo, renderingMode: .alwaysOriginal), style: .plain, target: self, action: #selector(settingsAtion))
        self.navigationItem.setRightBarButton(settingButton, animated: true)
    }
    
    @objc func settingsAtion() {
        // Go to setting VC
        let settingsVC = SettingsViewController()
        navigationController?.pushViewController(settingsVC, animated: true)
        
    }
    
    @objc func markYouPosition() {
        
        animateButton()
        
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        if (authorizationStatus == CLAuthorizationStatus.denied ) {
            
        }
        
        switch authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
            if let pinLocation = pinLocation {
                setLocation(pinLocation: pinLocation)
            }
            
        case .denied, .restricted:
            let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
            
            return
        @unknown default:
           
            break
        }
        
    }
    
    
    func setLocation(pinLocation: CLLocationCoordinate2D){
        
        if UserDefaults.standard.bool(forKey: "stopUpdate") == false{
                UserDefaults.standard.set(pinLocation.latitude, forKey: "curretnLatitude")
                UserDefaults.standard.set(pinLocation.longitude, forKey: "curretnLongitude")
                drawCircle(location: pinLocation)
            locationManager?.allowsBackgroundLocationUpdates = true
                markButtonView.titleLabel.text = NSLocalizedString("Remove location", comment: "")
                UserDefaults.standard.set(true, forKey: "stopUpdate")
        } else {
            markButtonView.titleLabel.text = NSLocalizedString("Mark my location", comment: "")
            UserDefaults.standard.set(0.0, forKey: "curretnLatitude")
            UserDefaults.standard.set(0.0, forKey: "curretnLongitude")
            let overlays = mapView.map.overlays
            locationManager?.allowsBackgroundLocationUpdates = false
            mapView.map.removeOverlays(overlays)
            UserDefaults.standard.set(false, forKey: "stopUpdate")
        }
        
    }
    
    
    func animateButton(){
        UIView.animate(withDuration: 0.1, animations: {
                  self.markButtonView.backgroundColor = UIColor.systemIndigo.withAlphaComponent(0.5)
              }, completion: { (wtf) in
                  UIView.animate(withDuration: 0.1) {
                      self.markButtonView.backgroundColor = UIColor.systemIndigo.withAlphaComponent(0.8)
                  }
              })
    }
    
    func drawCircle(location: CLLocationCoordinate2D){
        let overlays = mapView.map.overlays
        mapView.map.removeOverlays(overlays)
        let circle = MKCircle(center: location, radius: radius)
        self.mapView.map.addOverlay(circle)
        
        
        let geofanceRegion = CLCircularRegion(center: location, radius: radius, identifier: "notifyonexit")
        geofanceRegion.notifyOnExit = true
        geofanceRegion.notifyOnEntry = false
        locationManager?.startMonitoring(for: geofanceRegion)
        
    }
    
    
    
//    func createLocalNotification(message: String, identifier: String) {
//        //Create a local notification
//        let content = UNMutableNotificationContent()
//        content.body = message
//        content.sound = UNNotificationSound.default
//
//        // Deliver the notification
//        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: nil)
//
//        // Schedule the notification
//        let center = UNUserNotificationCenter.current()
//        center.add(request) { (error) in
//        }
//    }

    

}





