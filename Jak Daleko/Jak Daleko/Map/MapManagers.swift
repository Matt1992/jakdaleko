//
//  MapManagers.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 14/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


extension MapViewController: CLLocationManagerDelegate{
    // Set permision
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
     
        
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {

        } else {
            // IF DISABLED
            let alertController = UIAlertController(title: "JAK DALEKO", message: NSLocalizedString("Jak Daleko does need access to your location to be able to determine the center of the beam for safe walking", comment: ""), preferredStyle: .alert)
            
            let acceptAction = UIAlertAction(title: "Sure, I want to walk safe", style: .default) { (_) in
                self.locationManager?.requestAlwaysAuthorization()
            }
            let cancelAction = UIAlertAction(title: "No, I dont want to walk safe", style: .destructive, handler: nil)
            
            let image = UIImage(named: "my_logo-2")
            
            alertController.addImage(image: image!)
            
            alertController.addAction(acceptAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)

        }
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
       
    }


    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {return}
        let locationCoordinate = location.coordinate

        // Set Zoom to user location using distance in KM
        if spanRegion {
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude), latitudinalMeters: CLLocationDistance(distanceInKM*1000), longitudinalMeters: CLLocationDistance(distanceInKM*1000))
            mapView?.map.setRegion(region, animated: true)
            spanRegion = false 
        }
        
        pinLocation = locationCoordinate
    
        
    }
    

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print("Failed to find user's location: \(error.localizedDescription)")
     }
    

    // called when user Exits a monitored region
      func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
          if region is CLCircularRegion {
              // Do what you want if this information
//              self.createLocalNotification(message: "Wracaj bałwanie", identifier: "local_push")
          }
      }

}




extension MapViewController: MKMapViewDelegate {
    
  
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.systemIndigo
            circle.fillColor = UIColor.systemIndigo.withAlphaComponent(0.3)
            circle.lineWidth = 1
            return circle
        } else {
            return MKOverlayRenderer()
        }
    }
   
}




