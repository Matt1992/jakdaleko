//
//  PinModel.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 14/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import Foundation


struct Pin {
    var latitude: Double
    var longitude: Double
    var radius: Double
}
