//
//  Helper.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 17/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit



extension UIAlertController {
    
    func addImage(image: UIImage){
        let imageAction = UIAlertAction(title: "", style: .default, handler: nil)
        imageAction.isEnabled = false
        
         let left = view.frame.size.width/2 - image.size.width-5
        let centeredTopoImage = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: left, bottom: 0, right: 0)).withRenderingMode(.alwaysOriginal)
        imageAction.setValue(centeredTopoImage, forKey: "image")
        self.addAction(imageAction)
    }
}



