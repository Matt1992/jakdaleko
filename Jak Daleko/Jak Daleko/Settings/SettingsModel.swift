//
//  File.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 16/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import Foundation

enum MapType:Int {
    case standard
    case mutedStandard
    case hybrid
    case satellite
}

class SettingsModel {
    
    var mapType: MapType
    var desclimer: String
    var about: String
    var radius: Double
    
    init(mapType: MapType, desclimer: String, about: String, radius: Double) {
        self.mapType = mapType
        self.desclimer = desclimer
        self.about = about
        self.radius = radius
    }
}
