//
//  SettingsTableView.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 15/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit



extension SettingsViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return settingsViewModel.items[section].title
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settingsViewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsViewModel.items[section].numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = settingsViewModel.items[indexPath.section]
        
        switch item.type {
        case .mapT:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "map_type", for: indexPath) as? MapTypeCell {
                cell.delegate = self 
                return cell
            }
        case .radius:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "radius_cell", for: indexPath) as? MapRadiusCell {
              
                return cell
            }
            
        case .about:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "about_cell", for: indexPath) as? AboutTypeCell {
                return cell
            }
            
        case .desclimer:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "disclaimer_cell", for: indexPath) as? DisclaimerTypeCell {
                return cell
            }
            
        
        }
            
        return UITableViewCell()
    }
    
    
}

extension SettingsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = settingsViewModel.items[section].title
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 40))
        let label = UILabel(frame: CGRect(x: 8, y: 0, width: view.bounds.width, height: 30))
        label.text = title
        headerView.backgroundColor = .systemGray5
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = settingsViewModel.items[indexPath.section].type
        let infoVC = InfoViewController()
        switch item {
        case .desclimer:
            infoVC.settingsType = .desclimer
            navigationController?.pushViewController(infoVC, animated: true)
        case .about:
            infoVC.settingsType = .about
            navigationController?.pushViewController(infoVC, animated: true)
        default:
            return
        }
        
    }
}


