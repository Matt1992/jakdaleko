//
//  SettingsViewModel.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 16/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import Foundation


enum SettingsViewModelType {
    case mapT
    case desclimer
    case about
    case radius
}

protocol SettingsViewModelProtocol {
    var type: SettingsViewModelType { get }
    var numberOfRows: Int { get }
    var title: String { get }
}

extension SettingsViewModelProtocol {
    var numberOfRows: Int {
        return 1
    }
}

class SettingsViewModelMapType: SettingsViewModelProtocol {
    
    
    
    
    var type: SettingsViewModelType {
        return .mapT
    }
    
    var title: String {
        return NSLocalizedString("Map Type", comment: "") 
    }
    
    var mapType: MapType
    
    init(mapType: MapType) {
        self.mapType = mapType
    }
    
}


class SettingsViewModelDesclimer: SettingsViewModelProtocol {
    var type: SettingsViewModelType {
        return .desclimer
    }
    
    var title: String {
        return NSLocalizedString("Disclaimer", comment: "")
    }
    
    var desclimer: String
    
    init(desclimer: String) {
        self.desclimer = desclimer
    }
    
}


class SettingsViewModelAbout: SettingsViewModelProtocol {
    var type: SettingsViewModelType {
        return .about
    }
    
    var title: String {
        return NSLocalizedString("About", comment: "")
    }
    
    var about: String
    
    init(about: String) {
        self.about = about
    }
    
}

class SettingsViewModelRadius: SettingsViewModelProtocol {
    var type: SettingsViewModelType {
        return .radius
    }
    
    var title: String {
        return NSLocalizedString("Radius", comment: "")
    }
    
    var radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
}





class SettingsViewModel: NSObject {
    
    var items = [SettingsViewModelProtocol]()
    
    override init() {
        let settings = SettingsModel(mapType: .satellite, desclimer: "Disclaimer", about: "About", radius: 2000.0)
        
        let settingsViewModelMapType = SettingsViewModelMapType(mapType: settings.mapType)
        items.append(settingsViewModelMapType)
        
        let settingsViewModelRadius = SettingsViewModelRadius(radius: settings.radius)
        items.append(settingsViewModelRadius)
        
        let settingsViewModelAbout = SettingsViewModelAbout(about: settings.about)
        items.append(settingsViewModelAbout)
        
        let settingsViewModelDesclimer = SettingsViewModelDesclimer(desclimer: settings.desclimer)
        items.append(settingsViewModelDesclimer)
        
        
        
    }
    
}
