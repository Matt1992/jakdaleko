//
//  SettingsView.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 15/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit


class SettingsView: UIView{
    
    
    let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = .systemGray5
        table.bounces = false 
        return table
    }()
    
    let aboutAuthor: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Made with ❤️ in Sopot, Poland by Matt"
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .systemGray5
        setConstraints()
    }
    
    
    func setConstraints(){
        addSubview(tableView)
        addSubview(aboutAuthor)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.heightAnchor.constraint(equalToConstant: 400),
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            
            aboutAuthor.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            aboutAuthor.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            aboutAuthor.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 30),
        
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}





// MARK: Cell View


protocol MapTypeCellDelegate:AnyObject {
    func changeValue(selector: UISegmentedControl)
}

class MapTypeCell: UITableViewCell {
    
    
        
    let mapTypeSegment: UISegmentedControl = {
        let items = ["standard", "mutated", "hybrid", "satellite"]
        let segmentControl = UISegmentedControl(items: items)
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        segmentControl.selectedSegmentIndex = MapType(rawValue: UserDefaults.standard.integer(forKey: "mapType"))!.rawValue
        return segmentControl
    }()
    
    weak var delegate: MapTypeCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        selectionStyle = .none
        mapTypeSegment.addTarget(self, action: #selector(changedMap), for: .valueChanged)
    }
    
    @objc func changedMap(){
        delegate?.changeValue(selector: mapTypeSegment)
    }
    
    
    func setConstraints(){
        addSubview(mapTypeSegment)
        
        NSLayoutConstraint.activate([
            
            mapTypeSegment.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            mapTypeSegment.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            mapTypeSegment.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            mapTypeSegment.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}





class MapRadiusCell: UITableViewCell {
    
    
        
    let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = NSLocalizedString("Radius [m]", comment: "")
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    let radiusField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        if UserDefaults.standard.double(forKey: "radius") > 0.0 {
            textField.text = "\(UserDefaults.standard.double(forKey: "radius"))"
        } else {
            textField.text = "2000.0"
        }
        
        textField.keyboardType = .numberPad
        textField.returnKeyType = .done
        textField.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        return textField
    }()
    
    let containerView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.alignment = .center
        return stackView
    }()
    
    
    
    weak var delegate: MapTypeCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        selectionStyle = .none
        radiusField.addTarget(self, action: #selector(changedRadius), for: .editingDidEnd)
    }
    
    @objc func changedRadius(sender: UITextField){
        if let radiusText = sender.text {
            guard let radius = Double(radiusText), radius > 0.0 else { return }
            UserDefaults.standard.set(radius, forKey: "radius")
        }
        
    }
    
    
    func setConstraints(){
        containerView.addArrangedSubview(title)
        containerView.addArrangedSubview(radiusField)
        addSubview(containerView)
        
        NSLayoutConstraint.activate([
            
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}






class DisclaimerTypeCell: UITableViewCell {
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = NSLocalizedString("Read about disclaimer", comment: "")
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return titleLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        selectionStyle = .none
    }
    
    func setConstraints(){
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class AboutTypeCell: UITableViewCell {
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = NSLocalizedString("Read about project", comment: "") 
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return titleLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setConstraints()
        selectionStyle = .none
    }
    
    func setConstraints(){
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
        
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
