//
//  SettingsViewController.swift
//  Jak Daleko
//
//  Created by Mateusz Kopacz on 15/04/2020.
//  Copyright © 2020 Mateusz Kopacz. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    var settingsView: SettingsView!
    let settingsViewModel = SettingsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        settingsView = SettingsView(frame: view.frame)
        settingsView.tableView.delegate = self
        settingsView.tableView.dataSource = self
        settingsView.tableView.register(MapTypeCell.self, forCellReuseIdentifier: "map_type")
        settingsView.tableView.register(MapRadiusCell.self, forCellReuseIdentifier: "radius_cell")
        settingsView.tableView.register(DisclaimerTypeCell.self, forCellReuseIdentifier: "disclaimer_cell")
        settingsView.tableView.register(AboutTypeCell.self, forCellReuseIdentifier: "about_cell")
        
        
        view.addSubview(settingsView)
        
        navigationController?.navigationBar.tintColor = UIColor.systemIndigo
        
        settingsView.tableView.tableFooterView = UIView()
        
      
    }
    
  

}










extension SettingsViewController: MapTypeCellDelegate{
    func changeValue(selector: UISegmentedControl) {
        
        UserDefaults.standard.set(selector.selectedSegmentIndex, forKey: "mapType")
        
    }

    
    
}
